package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {

	// caputre your input with white line //
	// input: peter cherry john samson sam
	consoleReader1 := bufio.NewReader(os.Stdin)
	fmt.Println("Please enter your string array1")
	input1, _ := consoleReader1.ReadString('\n')

	// input: vincent dicky sam jo yiu
	consoleReader2 := bufio.NewReader(os.Stdin)
	fmt.Println("Please enter your string array2")
	input2, _ := consoleReader2.ReadString('\n')

	// convert to string to slice
	strArray1 := strings.Fields(input1)
	strArray2 := strings.Fields(input2)

	fmt.Println(strArray1)
	fmt.Println(strArray2)

	// fmt.Printf("strArray type = %T\n", strArray1)
	// fmt.Printf("strArray value = %s\n", strArray1)

	// count number element in array1
	var check1 bool
	for i := 0; i < len(strArray1); i++ {

		check1 = false
		var tmpChar = strArray1[i]
		// fmt.Println(strArray1[i])

		// loop by each char in an array
		for j := 0; j < len(strArray1[i]); j++ {

			//fmt.Println(string(tmpChar[j]))

			// loop each element in another array2
			for x := 0; x < len(strArray2); x++ {
				// find any char in array2
				if strings.Contains(strArray2[x], string(tmpChar[j])) == true {
					fmt.Printf("Matching some substring, %s and %s\n", strArray2[x], string(tmpChar[j]))
					check1 = true
					break
				} else {
					fmt.Printf("Not Matching some substring, %s and %s\n", strArray2[x], string(tmpChar[j]))

				}
			}

			if check1 == true {
				break
			}

		}

	}

}
