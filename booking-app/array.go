package main

import (
	"fmt"
)

func main() {
	fmt.Printf("Enter size of your array: ")
	var size int
	fmt.Scanln(&size)
	var arr = make([]string, size)

	fmt.Scanf("%s", &arr)

	fmt.Println("Your array is: ", arr)
}
