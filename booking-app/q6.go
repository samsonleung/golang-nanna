package main

import (
	"fmt"
	"strings"
)

func main() {
	var check1 bool = false

	var input = "kate,Kate,hongting,samson,Hongting1,hongting"

	// convert to string to slice with specific char
	strArray := strings.Split(input, ",")

	fmt.Printf("The len is %d\n", len(strArray))

	for x := 0; x < len(strArray); x++ {
		//fmt.Println(strArray[x])

		for y := x + 1; y < len(strArray); y++ {
			fmt.Println("x: ", strArray[x], "y: ", strArray[y])
			if strings.Compare(strArray[x], strArray[y]) == 0 {
				fmt.Println("The first repeated word is", strArray[y])
				check1 = true
				break
			}
		}

		if check1 == true {
			break
		}

	}

}
