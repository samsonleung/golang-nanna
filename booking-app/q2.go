package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	// input: samson sam peter hisam //
	var num1 int
	var var1 string
	fmt.Println("Please enter your search word")
	fmt.Scanln(&var1)

	// caputre your input with white line //
	consoleReader := bufio.NewReader(os.Stdin)
	fmt.Println("Please enter your string")
	input, _ := consoleReader.ReadString('\n')

	// convert to string to slice
	strArray := strings.Fields(input)

	fmt.Println("The len is %s", len(strArray))

	for x := 0; x < len(strArray); x++ {
		if strings.HasPrefix(strArray[x], var1) == true && strings.Compare(strArray[x], var1) != 0 {
			num1++
		}
	}
	fmt.Println(num1) //

}
