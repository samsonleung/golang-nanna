package main

import (
	"fmt"
)

func main() {
	/*
		for i, c := range "abc" {
			fmt.Println(i, " => ", string(c))
		}
	*/

	var num1 int
	var inputString string
	var outputString string

	inputString = "(((()((("
	outputString = ""
	num1 = 0

	for _, c := range inputString {
		/* fmt.Print(i, " => ", string(c)) */

		if string(c) == "(" {
			outputString = outputString + "("
			num1++

		} else {

			for x := 0; x < num1; x++ {
				outputString = outputString + ")"
			}
			num1 = 0
		}
	}

	// fmt.Println("The number is", num1)
	for x := 0; x < num1; x++ {
		outputString = outputString + ")"
	}

	fmt.Print(outputString)
}
