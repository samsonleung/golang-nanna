package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

/*
 * Complete the 'miniMaxSum' function below.
 *
 * The function accepts INTEGER_ARRAY arr as parameter.
 */

func miniMaxSum(arr []int32) {

	/* fmt.Println(len(arr)) */

	sum := 0

	for i := 0; i < len(arr); i++ {
		/* fmt.Printf("First loop, the value is %d. ", arr[i]) */

		for x := i + 1; x < len(arr); x++ {
			/* fmt.Printf("second loop, the value is %d. ", arr[x]) */

			if arr[i] == arr[x] {
				sum++

				break
			}

		}
	}

	fmt.Print(sum)

}

func main() {

	reader := bufio.NewReaderSize(os.Stdin, 16*1024*1024)

	arrTemp := strings.Split(strings.TrimSpace(readLine(reader)), " ")

	var arr []int32

	for i := 0; i < 10; i++ {
		arrItemTemp, err := strconv.ParseInt(arrTemp[i], 10, 64)
		checkError(err)
		arrItem := int32(arrItemTemp)
		arr = append(arr, arrItem)
	}

	miniMaxSum(arr)
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
